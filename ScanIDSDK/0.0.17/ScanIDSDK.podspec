#
#  Be sure to run `pod spec lint scansdk.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "ScanIDSDK"
  spec.version      = "0.0.17"
  spec.summary      = "This library is used to scan different ID Documents."
  spec.homepage     = "https://gitlab.com/frslabs-public/ios/scanidsdk"
  spec.license      = "MIT"
  spec.author       = { "Ashish" => "ashish@frslabs.com" }
  spec.platform     = :ios, "12.0"
  spec.source       = { :http => 'https://manage.repo.frslabs.space/repository/cocoapod-sdk-scanid/v0.0.17/ScanIDSDK.framework.zip'}
  spec.ios.vendored_frameworks = 'ScanIDSDK.framework'
  spec.swift_version = '5.0'
 # spec.resources = 'Pod/Assets/*'
 # spec.resource_bundles = {'ScanIDSDK' => ['Pod/Assets/*.png']}
 
  
  


end
